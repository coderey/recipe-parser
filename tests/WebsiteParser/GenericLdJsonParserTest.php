<?php

namespace CodereyTests\RecipeParser\WebsiteParser;

use Coderey\RecipeParser\WebsiteParser\GenericLdJsonParser;
use Coderey\RecipeStructure\Recipe;
use CodereyTests\RecipeParser\TestCase;

class GenericLdJsonParserTest extends TestCase
{
    /**
     * @covers \Coderey\RecipeParser\WebsiteParser\GenericLdJsonParser
     * @covers \Coderey\RecipeParser\StructuredDataParser\LdJsonParser
     * @covers \Coderey\RecipeParser\RecipeSchemaParser
     * @covers \Coderey\RecipeParser\RecipeParserAbstract
     *
     * @return void
     */
    public function testParsingChefkoch()
    {
        $parser = new GenericLdJsonParser();
        $recipe = $parser->parseUrl('https://www.chefkoch.de/rezepte/866211192089377/Muffins-mit-Schokosplittern.html')->getRecipe();

        $this->assertInstanceOf(Recipe::class, $recipe);
        $this->assertEquals('https://www.chefkoch.de/rezepte/866211192089377/Muffins-mit-Schokosplittern.html', $recipe->getOrigin());
        $this->assertEquals('Muffins mit Schokosplittern', $recipe->getTitle());
        $categories = $recipe->getCategories();
        $this->assertContains('Schnell und einfach', $categories);
        $this->assertCount(9, $recipe->getIngredients());
        $this->assertCount(1, $recipe->getInstructions());
        $this->assertStringContainsString('Das Mehl mit Puddingpulver und Backpulver mischen', $recipe->getInstructions()[0]);
        $this->assertStringContainsString('Den Teig in Muffinförmchen füllen und bei 160°C (Umluft) ca. 25 Minuten backen.', $recipe->getInstructions()[0]);
    }

}