<?php

namespace CodereyTests\RecipeParser\WebsiteParser;

use Coderey\RecipeParser\WebsiteParser\ChefkochParser;
use Coderey\RecipeParser\WebsiteParser\FirstInFoodParser;
use Coderey\RecipeParser\WebsiteParser\GenericLdJsonParser;
use Coderey\RecipeParser\WebsiteParser\RecipeFromUrlParserFactory;
use CodereyTests\RecipeParser\TestCase;
use Illuminate\Support\ItemNotFoundException;

class RecipeFromUrlParserFactoryTest extends TestCase
{

    public function urlToParserDataProvider(): array
    {
        return [
            ['https://www.chefkoch.de/rezepte/866211192089377/Muffins-mit-Schokosplittern.html',  ChefkochParser::class],
            ['https://www.eatbetter.de/rezepte/lachsnudeln-blitzschnell-cremig', FirstInFoodParser::class],
            ['https://www.einfachbacken.de/rezepte/schokomuffins-saftig-schnell', FirstInFoodParser::class],
            ['https://www.daskochrezept.de/rezepte/quiche-lorraine-das-beste-original-rezept', FirstInFoodParser::class],
            ['https://www.meine-familie-und-ich.de/rezepte/orangen-lava-kuechlein-mit-fluessigem-kern', FirstInFoodParser::class],
            ['https://www.gutekueche.at/lachsnudeln-rezept-1375', GenericLdJsonParser::class],
        ];
    }

    /**
     * @covers \Coderey\RecipeParser\WebsiteParser\RecipeFromUrlParserFactory
     * @dataProvider urlToParserDataProvider
     *
     * @param string $url
     * @param string $expectedParser
     *
     * @return void
     */
    public function testGetCorrectParser(string $url, string $expectedParser)
    {
        $instance = RecipeFromUrlParserFactory::parseUrl($url);
        $this->assertInstanceOf($expectedParser, $instance);
    }

    /**
     * @covers \Coderey\RecipeParser\WebsiteParser\RecipeFromUrlParserFactory
     * @return void
     */
    public function testGettingParserWillFailWithInvalidArgumentExceptionWhenThereIsNoUrl()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Could not parse URL "this is no URL"');
        RecipeFromUrlParserFactory::parseUrl('this is no URL');
    }

    /**
     * @covers \Coderey\RecipeParser\WebsiteParser\RecipeFromUrlParserFactory
     * @return void
     */
    public function testGettingParserWillFailWithItemNotFoundExceptionWhenThereIsNoLdJsonScriptWithTypeRecipe()
    {
        $this->expectException(ItemNotFoundException::class);
        $this->expectExceptionMessage('no ld+json recipe struct found');
        RecipeFromUrlParserFactory::parseUrl('https://www.chefkoch.de/');
    }

    /**
     * @covers \Coderey\RecipeParser\WebsiteParser\RecipeFromUrlParserFactory
     * @return void
     */
    public function testGettingParserWillFailWithItemNotFoundExceptionWhenThereIsNoLdJsonScript()
    {
        $this->expectException(ItemNotFoundException::class);
        $this->expectExceptionMessage('No Parser found for "https://www.knipserey.de/de/"');
        RecipeFromUrlParserFactory::parseUrl('https://www.knipserey.de/de/');
    }

    /**
     * @covers \Coderey\RecipeParser\WebsiteParser\RecipeFromUrlParserFactory
     * @return void
     */
    public function testGetSupportedWebsites()
    {
        $websites = RecipeFromUrlParserFactory::getSupportedWebsites();

        $this->assertContains('chefkoch.de', $websites);
        $this->assertContains('eatbetter.de', $websites);
        $this->assertContains('einfachbacken.de', $websites);
        $this->assertContains('meine-familie-und-ich.de', $websites);
        $this->assertContains('daskochrezept.de', $websites);
    }

}