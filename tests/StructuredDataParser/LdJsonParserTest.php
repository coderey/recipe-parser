<?php

namespace CodereyTests\RecipeParser\StructuredDataParser;

use Coderey\RecipeParser\StructuredDataParser\LdJsonParser;
use CodereyTests\RecipeParser\TestCase;

class LdJsonParserTest extends TestCase
{
    /**
     * if page has multiple ld+json script-tags
     *
     * @covers \Coderey\RecipeParser\StructuredDataParser\LdJsonParser
     * @return void
     */
    public function testParseLdJsonFromChefkochRecipePage()
    {
        $html    = file_get_contents(__DIR__ . '/../testContent/ChefkochRecipePage.html');

        //parse any ld+json
        $ldJsons = LdJsonParser::parse($html);
        $this->assertCount(2, $ldJsons);
        foreach ($ldJsons as $json) {
            $this->checkLdJson($json);
        }

        $ldJsons = LdJsonParser::parse($html, ['Recipe']);
        $this->assertCount(1, $ldJsons);
        foreach ($ldJsons as $json) {
            $this->checkLdJson($json);
        }
    }

    /**
     * if "@type=recipe" is inside "@graph"
     *
     * @covers \Coderey\RecipeParser\StructuredDataParser\LdJsonParser
     * @return void
     */
    public function testParseLdJsonFromEinfachBackenRecipePage()
    {
        $html    = file_get_contents(__DIR__ . '/../testContent/EinfachBackenRecipePage.html');

        //parse any ld+json
        $ldJsons = LdJsonParser::parse($html);
        $this->assertCount(1, $ldJsons);
        foreach ($ldJsons as $json) {
            $this->checkLdJson($json);
        }

        $ldJsons = LdJsonParser::parse($html, ['Recipe']);
        $this->assertCount(1, $ldJsons);
        foreach ($ldJsons as $json) {
            $this->checkLdJson($json);
        }
    }


    /**
     * if "@type=recipe" together with other structures in a single ld+json-script-tag
     *
     * @covers \Coderey\RecipeParser\StructuredDataParser\LdJsonParser
     * @return void
     */
    public function testParseLdJsonFromGuteKuecheRecipePage()
    {
        $html    = file_get_contents(__DIR__ . '/../testContent/GuteKuecheRecipePage.html');

        //parse any ld+json
        $ldJsons = LdJsonParser::parse($html);
        $this->assertCount(2, $ldJsons);
        foreach ($ldJsons as $json) {
            $this->checkLdJson($json);
        }

        $ldJsons = LdJsonParser::parse($html, ['Recipe']);
        $this->assertCount(1, $ldJsons);
        foreach ($ldJsons as $json) {
            $this->checkLdJson($json);
        }
    }

    protected function checkLdJson(mixed $json)
    {
        $this->assertIsArray($json);
        $this->assertArrayHasKey('@type', $json);
    }

}