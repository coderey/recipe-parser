<?php

namespace Coderey\RecipeParser;

use Coderey\RecipeStructure\Recipe;
use Coderey\RecipeStructure\RecipeInterface;

abstract class RecipeParserAbstract implements RecipeParserInterface
{
    protected RecipeInterface $recipe;

    public function __construct()
    {
        $this->recipe = new Recipe();
    }

    /**
     * @param string $recipeText
     *
     * @return RecipeParserInterface
     */
    abstract public function parseText(string $recipeText): RecipeParserInterface;

    /**
     * @return RecipeInterface
     */
    public function getRecipe(): RecipeInterface
    {
        return $this->recipe;
    }
}
