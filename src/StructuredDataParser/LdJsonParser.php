<?php

namespace Coderey\RecipeParser\StructuredDataParser;

use Illuminate\Support\Arr;

class LdJsonParser
{
    public static function parse($html, array $types = []): array
    {
        $results = [];
        $regex   = '/<script[^>]+type="application\/ld\+json">(.*)<\/script>/isU';
        if (preg_match_all($regex, $html, $out)) {
            foreach ($out[1] as $json) {
                $result = json_decode($json, true);
                if (array_key_exists('@type', $result)) {
                    $results[] = $result;
                } elseif (array_key_exists('@graph', $result)
                    && is_array($result['@graph'])
                ) {
                    $resultArray = $result['@graph'];
                    foreach ($resultArray as $subResult) {
                        if (array_key_exists('@type', $subResult)) {
                            $results[] = $subResult;
                        }
                    }
                } elseif (Arr::isList($result)) {
                    foreach ($result as $subResult) {
                        if (array_key_exists('@type', $subResult)) {
                            $results[] = $subResult;
                        }
                    }
                }
            }
        }

        if (!empty($types)) {
            $results = array_filter($results, function($item) use ($types) {
                return array_key_exists('@type', $item) && in_array($item['@type'], $types);
            });
        }

        return $results;
    }
}
