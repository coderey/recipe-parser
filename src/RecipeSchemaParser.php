<?php

namespace Coderey\RecipeParser;

use Coderey\RecipeParser\StructuredDataParser\LdJsonParser;
use Illuminate\Support\Arr;
use Illuminate\Support\ItemNotFoundException;
use DateInterval;
use Exception;

class RecipeSchemaParser extends RecipeParserAbstract implements RecipeParserInterface
{
    public function parseText(string $recipeText): RecipeParserInterface
    {
        $ldJsons = LdJsonParser::parse($recipeText, ['Recipe']);
        if (count($ldJsons) !== 1) {
            throw new ItemNotFoundException('no ld+json recipe struct found');
        }
        $json = array_pop($ldJsons);

        if (array_key_exists('recipeCategory', $json)) {
            $this->addCategory($json['recipeCategory']);
        }

        if (array_key_exists('recipeIngredient', $json) && is_array($json['recipeIngredient'])) {
            foreach ($json['recipeIngredient'] as $ingredient) {
                $this->recipe->addIngredient($ingredient);
            }
        }

        if (array_key_exists('name', $json)) {
            $this->recipe->setTitle($json['name']);
        }

        if (array_key_exists('recipeInstructions', $json)) {
            $this->addInstruction($json['recipeInstructions']);
        }

        if (array_key_exists('image', $json)) {
            $this->addImage($json['image']);
        }

        if (array_key_exists('totalTime', $json)) {
            $this->recipe->setTotalTime($this->intervalInMinutes($json['totalTime']));
        }
        if (array_key_exists('prepTime', $json)) {
            $this->recipe->setWorkingTime($this->intervalInMinutes($json['prepTime']));
        }
        if (array_key_exists('cookTime', $json)) {
            $this->recipe->setCookingTime($this->intervalInMinutes($json['cookTime']));
        }

        return $this;
    }

    protected function addImage($img)
    {
        if (is_string($img) && $this->isUrl($img)) {
            //simple image URL
            $this->recipe->addImage($img);
        } elseif (is_array($img) && array_key_exists('@type', $img) && array_key_exists('url', $img)) {
            //structured data image
            $this->recipe->addImage($img['url']);
        } elseif (is_array($img)) {
            //maybe multiple images
            foreach ($img as $item) {
                $this->addImage($item);
            }
        }
    }

    protected function addCategory($cat)
    {
        if (is_string($cat)) {
            $this->recipe->addCategory($cat);
        } elseif (is_array($cat) && Arr::isList($cat)) {
            foreach ($cat as $subcat) {
                $this->addCategory($subcat);
            }
//        } else {
//            dd($cat);
        }
    }

    protected function addInstruction($instruction)
    {
        if (is_string($instruction)) {
            $this->recipe->addInstruction($instruction);
        } elseif (is_array($instruction) && Arr::isList($instruction)) {

            foreach ($instruction as $text) {
                $this->addInstruction($text);
            }
        } elseif (is_array($instruction) && array_key_exists('@type', $instruction) && array_key_exists('text', $instruction)) {
            $this->addInstruction($instruction['text']);
        }
    }

    protected function isUrl($url): bool
    {
        return preg_match('/^https?:\/\//i', $url);
    }

    /**
     * @param string $intervalString
     * @return int
     *
     * @throws Exception
     */
    protected function intervalInMinutes(string $intervalString): int
    {
        $interval = new DateInterval($intervalString);
        $minutes  = $interval->i;
        $minutes  += ($interval->h * 60);
        $minutes  += ($interval->d * 60 * 24);

        return $minutes;
    }

}
