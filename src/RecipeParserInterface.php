<?php

namespace Coderey\RecipeParser;

use Coderey\RecipeStructure\RecipeInterface;

interface RecipeParserInterface
{
    public function parseText(string $recipeText): self;
    public function getRecipe(): RecipeInterface;
}
