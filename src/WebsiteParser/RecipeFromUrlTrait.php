<?php

namespace Coderey\RecipeParser\WebsiteParser;

use Coderey\RecipeParser\RecipeParserAbstract;
use GuzzleHttp\Client;

trait RecipeFromUrlTrait
{
    /**
     * @param string $url
     *
     * @return $this
     */
    public function parseUrl(string $url): RecipeFromUrlInterface
    {
        $this->recipe->setOrigin($url);
        $client = new Client();
        $response = $client->get($url);
        $this->parseText($response->getBody()->getContents());

        return $this;
    }
}
