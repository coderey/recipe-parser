<?php

namespace Coderey\RecipeParser\WebsiteParser;

use Coderey\RecipeParser\RecipeParserAbstract;
use Coderey\RecipeParser\RecipeParserInterface;

trait RecipeHtmlTrait
{
    /**
     * @param string $recipeText
     *
     * @return RecipeParserAbstract
     */
    protected function parsePageTitle(string $recipeText): RecipeParserAbstract
    {
        /** @var RecipeParserAbstract $this */
        if (preg_match('/<title>(.*)<\/title>/iU', $recipeText, $out)) {
            $this->recipe->setTitle($out[1]);
        }

        return $this;
    }

    /**
     * @param string $timeString
     * @return RecipeParserInterface
     */
    public function parseTime(string $timeString): RecipeParserInterface
    {
        $timeString = strtolower($timeString);

        if (preg_match('/^(?P<timeWord>.*)\s(?P<number>\d+)\s(?P<unit>(?:Min|Minuten?|Stunden?|Stdn?))/i', $timeString, $out)) {
            $minutes = 0;

            if (preg_match('/^(?P<timeWord>.*)\s(?P<number>\d+)\s(?P<unit>(?:Min|Minuten?))/i', $timeString, $outMin)) {
                $minutes += $outMin['number'];
            }
            if (preg_match('/^(?P<timeWord>.*)\s(?P<number>\d+)\s(?P<unit>(?:Stunden?|Stdn?))/i', $timeString, $outHour)) {
                $minutes += ($outHour['number']*60);
            }

            //Arbeitszeit
            if (str_contains($out['timeWord'], 'arbeitszeit')
            ) {
                $this->recipe->setWorkingTime($minutes);
                return $this;
            }

            //Koch-/Back-Zeit
            if (str_contains($out['timeWord'], 'kochzeit')
                || str_contains($out['timeWord'], 'backzeit')
                || str_contains($out['timeWord'], 'kochen')
                || str_contains($out['timeWord'], 'backen')
            ) {
                $this->recipe->setCookingTime($minutes);
                return $this;
            }


            //Koch-/Back-Zeit
            if (str_contains($out['timeWord'], 'kühlen')
                || str_contains($out['timeWord'], 'ruhezeit')
            ) {
                $this->recipe->setCoolingTime($minutes);
                return $this;
            }


            //Gesamt
            if (str_contains($out['timeWord'], 'gesamtzeit')
                || str_contains($out['timeWord'], 'zeit')
                || str_contains($out['timeWord'], 'dauer')
            ) {
                $this->recipe->setTotalTime($minutes);
            }
        }

        return $this;
    }

}