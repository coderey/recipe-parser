<?php

namespace Coderey\RecipeParser\WebsiteParser;

interface RecipeFromUrlInterface
{
    public function parseUrl(string $url): RecipeFromUrlInterface;
}