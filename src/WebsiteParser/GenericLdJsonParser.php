<?php

namespace Coderey\RecipeParser\WebsiteParser;

use Coderey\RecipeParser\RecipeParserInterface;
use Coderey\RecipeParser\RecipeSchemaParser;

class GenericLdJsonParser extends RecipeSchemaParser implements RecipeParserInterface, RecipeFromUrlInterface
{
    use RecipeFromUrlTrait;
    use RecipeHtmlTrait;

}