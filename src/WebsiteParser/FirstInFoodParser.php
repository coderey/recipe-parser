<?php

namespace Coderey\RecipeParser\WebsiteParser;

use Coderey\RecipeParser\RecipeParserInterface;

class FirstInFoodParser extends GenericLdJsonParser
{
    public function parseText(string $recipeText): RecipeParserInterface
    {
        parent::parseText($recipeText);

        //FALLBACK: title
        if (!$this->recipe->getTitle()) {
            $this->parsePageTitle($recipeText);
        }

        $meta = $this->parseMeta($recipeText);

        foreach ($meta as $key => $value) {
            if ($key == 'Niveau') {
                $this->parseDifficulty($value);
            } else {
                $this->parseTime($key . ' ' . $value);
            }
        }

        return $this;
    }

    protected function parseMeta(string $recipeText): array
    {
        $result = [];
        $regex  = '/<div[^>]+recipe--heading__times-field[^>]+><div[^>]+recipe--heading__times-label[^>]+>(?P<label>[^<]+)<\/div><div[^>]+recipe--heading__times-value[^>]+>(?P<value>[^<]+)<\/div>\s*<\/div>/isU';
        if (preg_match_all($regex, $recipeText, $out)) {
            foreach ($out['label'] as $i => $label) {
                //remove icons
                $item = trim(strip_tags($out['value'][$i]));
                $result[$label] = $item;
            }
        }

        return $result;
    }

    protected function parseDifficulty(string $difficulty): self
    {
        switch ($difficulty) {
            case 'Einfach':
                $this->recipe->setDifficulty($this->recipe::DIFFICULTY_EASY);
                break;
            case 'Normal':
                $this->recipe->setDifficulty($this->recipe::DIFFICULTY_NORMAL);
                break;
            case 'Mittel':
                $this->recipe->setDifficulty($this->recipe::DIFFICULTY_MEDIUM);
                break;
            case 'Schwer': // hab kein schweres Rezept gefunden... keine Ahnung, ob es als "Schwer" da steht... ;-)
                $this->recipe->setDifficulty($this->recipe::DIFFICULTY_HARD);
                break;
        }

        return $this;
    }
}
