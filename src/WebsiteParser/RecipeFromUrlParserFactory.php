<?php

namespace Coderey\RecipeParser\WebsiteParser;

use Illuminate\Support\ItemNotFoundException;

class RecipeFromUrlParserFactory
{
    protected static $parser = [
        'chefkoch.de'              => ChefkochParser::class,
        'einfachbacken.de'         => FirstInFoodParser::class,
        'eatbetter.de'             => FirstInFoodParser::class,
        'meine-familie-und-ich.de' => FirstInFoodParser::class,
        'daskochrezept.de'         => FirstInFoodParser::class,
    ];

    public static function getSupportedWebsites(): array
    {
        return array_keys(self::$parser);
    }

    public static function parseUrl(string $url): RecipeFromUrlInterface
    {
        if (!preg_match('/^https?:\/\/(?P<domain>[^\/]+)\/.*$/i', $url, $out)) {
            throw new \InvalidArgumentException('Could not parse URL "' . $url . '"');
        }

        foreach (static::$parser as $site => $parser) {
            if (str_contains($out['domain'], $site)) {
                /** @var RecipeFromUrlInterface $instance */
                $instance = new $parser();
                $instance->parseUrl($url);

                return $instance;
            }
        }

        //try generic LdJson
        try {
            $instance = new GenericLdJsonParser();
            $instance->parseUrl($url);

            return $instance;

        } catch (ItemNotFoundException $e) {
            throw new ItemNotFoundException('No Parser found for "' . $url . '"');
        }
    }

}
