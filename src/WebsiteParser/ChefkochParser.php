<?php

namespace Coderey\RecipeParser\WebsiteParser;

use Coderey\RecipeParser\RecipeParserInterface;

class ChefkochParser extends GenericLdJsonParser
{
    public function parseText(string $recipeText): RecipeParserInterface
    {
        parent::parseText($recipeText);

        //ADDITIONAL-DATA: difficulty
        $this->parseDifficulty($recipeText);

        //FALLBACK: title
        if (!$this->recipe->getTitle()) {
            $this->parsePageTitle($recipeText);
        }

        //big parse-magic happens here...
        $split1 = explode('Zubereitung</h2>', $recipeText, 2);
        $split2 = explode('</div>', $split1[1], 2);

        //ADDITIONAL-DATA: times
        foreach ($this->parseMeta($split2[0], '') as $timing) {
            $this->parseTime($timing);
        }

        //FALLBACK: instructions
        if (!$this->recipe->getInstructions()) {
            $desc = preg_replace('/^.*<div[^>]*>/isU', '', $split2[0]);
            $desc = trim(strip_tags($desc));
            $this->recipe->addInstruction($desc);
        }

        return $this;
    }

    protected function parseMeta(string $recipeText, string $cssClass = ''): array
    {
        $result = [];
        $regex  = '/<span[^>]+' . (!empty($cssClass) ? $cssClass . '.*' : '')
                  . 'rds-recipe-meta__badge[^>]+>(.*)<\/span>/isU';
        if (preg_match_all($regex, $recipeText, $out)) {
            foreach ($out[1] as $item) {
                //remove icons
                $item = preg_replace('/<i.*>.*<\/i>/iU', '', $item);
                $item = trim(strip_tags($item));
                $result[] = $item;
            }
        }

        return $result;
    }

    protected function parseDifficulty(string $recipeText): self
    {
        $difficulty = $this->parseMeta($recipeText, 'recipe-difficulty');
        if (!empty($difficulty)) {
            switch ($difficulty[0]) {
                case 'simpel':
                    $this->recipe->setDifficulty($this->recipe::DIFFICULTY_EASY);
                    break;
                case 'normal':
                    $this->recipe->setDifficulty($this->recipe::DIFFICULTY_MEDIUM);
                    break;
                case 'pfiffig':
                    $this->recipe->setDifficulty($this->recipe::DIFFICULTY_HARD);
                    break;
            }
        }

        return $this;
    }
}
